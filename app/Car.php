<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    const DAY_PRICE = 'day_price';
    const CAR_PRICE = 'car_price';
    const BRAND = 'brand';
    const MODEL = 'model';
    const FUEL = 'fuels';
    const GEARBOX = 'gearbox';
    const COLOR = 'color';
    const YEAR = 'years';
    const RUN = 'run';
    const CONSUMPTION = 'consumption';
    const DESCRIPTION = 'description';
    const IMG = 'img';
    const AVAILABLE = 'available';

    protected $fillable = [
        self::DAY_PRICE,
        self::CAR_PRICE,
        self::BRAND,
        self::MODEL,
        self::FUEL,
        self::GEARBOX,
        self::COLOR,
        self::YEAR,
        self::RUN,
        self::CONSUMPTION,
        self::DESCRIPTION,
        self::IMG,
        self::AVAILABLE
    ];


    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }


    public function getDayPrice()
    {
        return $this->getAttribute(self::DAY_PRICE);
    }

    public function setDayPrice($val)
    {
        $this->setAttribute(self::CAR_PRICE, $val);
    }


    public function getBrand()
    {
        return $this->getAttribute(self::BRAND);
    }

    public function setBrand($val)
    {
        $this->setAttribute(self::BRAND, $val);
    }


    public function getModel()
    {
        return $this->getAttribute(self::MODEL);
    }

    public function setModel($val)
    {
        $this->setAttribute(self::MODEL, $val);
    }


    public function getFuel()
    {
        return $this->getAttribute(self::FUEL);
    }

    public function setFuel($val)
    {
        $this->setAttribute(self::FUEL, $val);
    }


    public function getGearbox()
    {
        return $this->getAttribute(self::GEARBOX);
    }

    public function setGearbox($val)
    {
        $this->setAttribute(self::GEARBOX, $val);
    }


    public function getColor()
    {
        return $this->getAttribute(self::COLOR);
    }

    public function setColor($val)
    {
        $this->setAttribute(self::COLOR, $val);
    }


    public function getYear()
    {
        return $this->getAttribute(self::YEAR);
    }

    public function setYear($val)
    {
        $this->setAttribute(self::YEAR, $val);
    }


    public function getRun()
    {
        return $this->getAttribute(self::RUN);
    }

    public function setRun($val)
    {
        $this->setAttribute(self::RUN, $val);
    }


    public function getConsumption()
    {
        return $this->getAttribute(self::CONSUMPTION);
    }

    public function setConsumption($val)
    {
        $this->setAttribute(self::CONSUMPTION, $val);
    }


    public function getDescription()
    {
        return $this->getAttribute(self::DESCRIPTION);
    }

    public function setDescription($val)
    {
        $this->setAttribute(self::DESCRIPTION, $val);
    }


    public function getImg()
    {
        return $this->getAttribute(self::IMG);
    }

    public function setImg($val)
    {
        $this->setAttribute(self::IMG, $val);
    }

    public function getAvailable()
    {
        return $this->getAttribute(self::AVAILABLE);
    }

    public function setAvailable($val)
    {
        $this->setAttribute(self::AVAILABLE, $val);
    }



}
