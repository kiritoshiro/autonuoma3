<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    const DATE_FROM = 'date_from';
    const DATE_TO = 'date_to';
    const PLACE_FROM = 'place_from';
    const PLACE_TO = 'place_to';
    const FK_CAR_ID = 'car_id';
    const STATUS = 'status';
    const PRICE = 'price';
    const DAY_COUNT = 'day_count';

    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const EMAIL = 'email';
    const CITY = 'city';
    const COUNTRY = 'country';
    const BIRTHDAY = 'birthday';
    const ADDRESS = 'address';
    const PHONE_NR = 'phone_nr';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::DATE_FROM,
        self::DATE_TO,
        self::PLACE_FROM,
        self::PLACE_TO,
        self::FK_CAR_ID,
        self::STATUS,
        self::PRICE,
        self::DAY_COUNT,

        self::FIRST_NAME,
        self::LAST_NAME,
        self::EMAIL,
        self::CITY,
        self::COUNTRY,
        self::BIRTHDAY,
        self::ADDRESS,
        self::PHONE_NR,
    ];

    public function cars(){
        return $this->hasOne(Car::class);
    }
    public function getDateFrom()
    {return $this->getAttribute(self::DATE_FROM);}

    public function setDateFrom($val)
    {$this->setAttribute(self::DATE_FROM, $val);}


    public function getDateTo()
    {return $this->getAttribute(self::DATE_TO);}

    public function setDateTo($val)
    {$this->setAttribute(self::DATE_TO, $val);}


    public function getPlaceFrom()
    {return $this->getAttribute(self::PLACE_FROM);}

    public function setPlaceFrom($val)
    {$this->setAttribute(self::PLACE_FROM, $val);}


    public function getPlaceTo()
    {return $this->getAttribute(self::PLACE_TO);}

    public function setPlaceTo($val)
    {$this->setAttribute(self::PLACE_TO, $val);}


    public function getCarId()
    {return $this->getAttribute(self::FK_CAR_ID);}

    public function setCarId($val)
    {$this->setAttribute(self::FK_CAR_ID, $val);}


    public function getStatus()
    {
        return $this->getAttribute(self::STATUS);
    }

    public function setStatus($val)
    {
        $this->setAttribute(self::STATUS, $val);
    }
}
