<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use App\Reservation;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Facades\App;

class ReservationController extends Controller
{

    public function create(Request $request)
    {
        $reservation = Reservation::create($request->all());
        return $reservation;
    }

    public function showAll()
    {
        $reservations = Reservation::latest()->paginate(10);
        return $reservations;
    }


    public function show($id)
    {
        $reservation = Reservation::findOrFail($id);
        return $reservation;
    }


    public function destroy($id)
    {
        $reservation = Reservation::findOrFail($id);
        $reservation->delete();
        return '';
    }

    public function update(Request $request, $id){

        $reservation = Reservation::findOrFail($id);
        $reservation->update($request->all());
        return $reservation;
    }
//    public function saved(Request $request){
//        $uniqueUrl = $this->getUnique();
//        $session_id = array_get($request, 'session_id');
//        $results = new Results();
//        $results->fill($request->toArray());
//
//        $results->setAttribute('result_url', $uniqueUrl);
//        $results->setAttribute('made_choice_id', $session_id);
//        $results->save();
//
//        $questions = MadeChoice::where('session_id', $session_id)->get();
//
//        return view('result', [
//            'username' => $request['username'],
//            'result_url' => $uniqueUrl,
//            'points' => $request['points'],
//            'questions' => $questions
//        ]);
//    }
//
//    public function getUnique() {
//        $uniqueUrl = rand(100000, 999999);
//        if(Results::where('result_url', $uniqueUrl)->first() != null) {
//            return $this->getUnique();
//        }
//        return $uniqueUrl;
//    }


}
