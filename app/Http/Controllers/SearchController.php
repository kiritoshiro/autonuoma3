<?php

namespace App\Http\Controllers;

use App\Car;
//use App\http\Controllers\CarSearch;
use Illuminate\Http\Request;


class SearchController extends Controller
{
    public function filter(Request $request)
    {

        $query = Car::latest();

        if ($request->has('brand')) {
            $query = $query->where('brand', $request->input('brand'));
        }
        if ($request->has('model')) {
            $query = $query->where('model', $request->input('model'));
        }
        if ($request->has('years')) {
            $query = $query->where('years', $request->input('years'));
        }
        if ($request->has('gearbox')) {
            $query = $query->where('gearbox', $request->input('gearbox'));
        }
        if ($request->has('color')) {
            $query = $query->where('color', $request->input('color'));
        }
        if ($request->has('fuels')) {
            $query = $query->where('fuels', $request->input('fuels'));
        }


        $query->latest()->first();

        $returnData = $query->paginate(10);

        return $returnData;
    }


}