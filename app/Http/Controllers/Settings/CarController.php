<?php

namespace App\Http\Controllers\Settings;

use App\Car;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CarController extends Controller
{
//    var $allCars;
    public function __construct()
    {
//        $this->middleware('auth');

//        $this->allCars = Car::all();
//        $this->middleware('log')->only('index');
//
//        $this->middleware('subscribed')->except('store');
    }

    public function showAll()
    {
        $cars = Car::paginate(10);
//        print_r($cars->toJson());
//        die();
        return $cars;
    }

    public function show($id)
    {
        $car = Car::findOrFail($id);
        return $car;
    }

    public function update(Request $request, $id)
    {
        $car = Car::findOrFail($id);
        $car->update($request->all());
        return $car;
    }

    public function uploadFile(Request $request)
    {
        $path = $request->file('file')->store('public');
        $path = str_replace('public/', '', $path);
//        dd($path);
        return $path;
    }

    public function store(Request $request)
    {
        $car = Car::create($request->all());
        return $car;
    }

    public function destroy($id)
    {
        $car = Car::findOrFail($id);

        Storage::delete($car->IMG);  /* delete photo from storage */
        $car->delete();

        return '';
    }
}
