# Autonuom3
Automobilių nuomos tinklalapis. Sukurtas naudojant Laravel, Vue-js.

### Steps to setup the project
1. ```git clone https://kiritoshiro@bitbucket.org/kiritoshiro/autonuoma3.git```
2. ```cp .env.dusk.testing .env```
3. Open .env file and setup database name with MySQL login credentials
4. ```npm install```
5. ```composer install```
6. ```php artisan key:generate```
7. ```php artisan migrate```
8. ```php artisan db:seed```

### To run project
1. ```php artisan serve```
2. ```http://127.0.0.1:8000/```

## Features
- Laravel 5.5 + Vue + Vue Router + Vuex
- Integration with [vform](https://github.com/cretueusebiu/vform)
- Authentication with [JWT](https://github.com/tymondesigns/jwt-auth)
- Webpack with [laravel-mix](https://github.com/JeffreyWay/laravel-mix)
- SVG icons with [svg-sprite-loader](https://github.com/kisenka/svg-sprite-loader)

#### Development

1. ```php artisan serve```
2. ```http://127.0.0.1:8000/```
3. ```npm run dev```
4. Run database

