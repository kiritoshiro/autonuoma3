<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Car;
use App\user;

use Illuminate\Foundation\Testing\WithoutMiddleware;


class CarTest extends TestCase
{
//    use WithoutMiddleware;

    /** @var \App\User */
    protected $user;


    public function setUp()
    {
        parent::setUp();

        $this->user = factory(\App\User::class)->create();
    }


    /** @test */
    public function cars_table_tests()
    {
        // POST
        $data = [
            Car::BRAND => 'Audi',
        ];
        $this->actingAs($this->user)
            ->postJson(route('car.create'), $data)
            ->assertSuccessful()
            ->assertJsonStructure(['id', 'brand',]);

        $this->assertDatabaseHas('cars', [
            'brand' => 'Audi',
        ]);

        // GET
        $this->getJson(route('cars.show'))
            ->assertSuccessful();

        // GET one
        $response =
            $this->actingAs($this->user)
                ->getJson(route('car.show',1) )
                ->assertSuccessful()
                ->assertJsonStructure(['id']);

        // DELETE
        $id = 1;
        $page = route('car.destroy', $id);
        $this->actingAs($this->user)
            ->deleteJson($page)
            ->assertSuccessful();
        $this->assertDatabaseMissing('cars', ['id' => $id]);

    }

}
