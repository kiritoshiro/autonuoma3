<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Reservation;

class ReservationTest extends TestCase
{
    /**
     * Reservation table tests.
     *
     * @return void
     */
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(\App\User::class)->create();
    }

    /** @test */
    public function reservations_table_tests()
    {
        // POST
        $data = [
            Reservation::FIRST_NAME => 'First Name',
            Reservation::LAST_NAME => 'Last Name',
            Reservation::PRICE => '100',
        ];
        $this->postJson('/api/car/1/reservation', $data)
            ->assertSuccessful()
            ->assertJsonStructure(['id', Reservation::FIRST_NAME, Reservation::LAST_NAME, Reservation::PRICE]);

        $this->assertDatabaseHas('reservations', [
            Reservation::FIRST_NAME => 'First Name',
            Reservation::LAST_NAME => 'Last Name',
            Reservation::PRICE => '100',
        ]);

        // GET all
        $response =
        $this->actingAs($this->user)
            ->getJson('/api/settings/reservations')
            ->assertSuccessful()
            ->assertJsonStructure(['data' => [
                ['id']
            ]]);

        // GET one
        $response =
        $this->actingAs($this->user)
            ->getJson('/api/reservation/info/1')
            ->assertSuccessful()
            ->assertJsonStructure(['id']);

        // DELETE
        $id = 1;
        $page = route('reservation.destroy', $id);
        $this->actingAs($this->user)
            ->deleteJson( $page)
            ->assertSuccessful();
        $this->assertDatabaseMissing('reservations', ['id' => $id]);

    }
}
