<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('home', 'SearchController@filter')->name('cars.show');
Route::post('car/{id}/reservation', 'ReservationController@create')->name('reservation.create');
Route::get('car/{id}', 'Settings\CarController@show')->name('car.show');

Route::get('reservation/info/{id}', 'ReservationController@show')->name('reservation.show');


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update')->name('profile.update');
    Route::patch('settings/password', 'Settings\PasswordController@update')->name('password.update');

    Route::post('settings/add', 'Settings\CarController@store')->name('car.create');
    Route::post('settings/add/uploadfile', 'Settings\CarController@uploadFile');

    Route::get('settings/overview', 'Settings\CarController@showAll')->name('cars.show');
    Route::delete('settings/overview/{id}', 'Settings\CarController@destroy')->name('car.destroy');

    Route::get('settings/edit/{id}', 'Settings\CarController@show')->name('car.show');
    Route::patch('settings/edit/{id}', 'Settings\CarController@update')->name('car.update');

    Route::get('settings/reservations', 'ReservationController@showAll')->name('reservations.show');
    Route::delete('settings/reservation/{id}', 'ReservationController@destroy')->name('reservation.destroy');
    Route::post('settings/reservation/{id}/status/{status}', 'ReservationController@updateStatus')->name('status.update');
    Route::get('settings/order/{id}', 'ReservationController@show')->name('reservation.show');
    Route::patch('settings/order/{id}', 'ReservationController@update')->name('reservation.update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('register', 'Auth\RegisterController@register')->name('register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});


