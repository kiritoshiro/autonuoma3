import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import {i18n} from '~/plugins'
import App from '~/components/App'

import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import 'vue-airbnb-style-datepicker/dist/styles.css'
const datepickerOptions = {}
// make sure we can use it in our components
Vue.use(AirbnbStyleDatepicker, datepickerOptions)

import '~/components'

Vue.config.productionTip = false

new Vue({
    i18n,
    store,
    router,
    ...App
})
