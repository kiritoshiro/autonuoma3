export default ({authGuard, guestGuard}) => [

    {path: '/', name: 'home', component: require('~/pages/home.vue')},
    {path: '/car/:id/reservation', name: 'reservation', component: require('~/pages/reservation.vue')},
    {path: '/car/:id', name: 'info', component: require('~/pages/info.vue')},
    {path: '/reservation/info/:id', name: 'reservation_info', component: require('~/pages/reservationInformation.vue')},
    // Authenticated routes.
    ...authGuard([
        {
            path: '/settings', component: require('~/pages/settings/index.vue'), children: [
                {path: '', redirect: {name: 'settings.profile'}},
                {path: 'profile', name: 'settings.profile', component: require('~/pages/settings/profile.vue')},
                {path: 'password', name: 'settings.password', component: require('~/pages/settings/password.vue')},
                {path: 'overview', name: 'settings.overview', component: require('~/pages/settings/overview.vue')},
                {path: 'edit/:id', name: 'settings.edit', component: require('~/pages/settings/edit.vue')},
                {path: 'order', name: 'settings.order', component: require('~/pages/settings/order.vue')},
                {path: 'order/:id', name: 'settings.updateReservation', component: require('~/pages/settings/updateReservation.vue')},
                {path: 'add', name: 'settings.add', component: require('~/pages/settings/add.vue')}
            ]
        }
    ]),

    // Guest routes.
    ...guestGuard([
        {path: '/login', name: 'login', component: require('~/pages/auth/login.vue')},
        {path: '/password/reset', name: 'password.request', component: require('~/pages/auth/password/email.vue')},
        {path: '/password/reset/:token', name: 'password.reset', component: require('~/pages/auth/password/reset.vue')}
    ]),
    {path: '*', component: require('~/pages/errors/404.vue')},
]
