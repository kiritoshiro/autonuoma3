<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('day_price')->nullable();
            $table->integer('car_price')->nullable();

            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('fuels')->nullable();
            $table->string('gearbox')->nullable();
            $table->string('color')->nullable();
            $table->integer('years')->nullable();
            $table->integer('run')->nullable();
            $table->integer('consumption')->nullable();
            $table->text('description')->nullable();
            $table->string('img')->nullable();

            $table->boolean('available')->default(true)->nullable();
            $table->date('notAvailableFrom')->nullable();
            $table->date('notAvailableTo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
