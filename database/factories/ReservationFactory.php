<?php

use Faker\Generator as Faker;
use App\Reservation;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Reservation::class, function (Faker $faker) {
    $place = [
        'Klaipėda, Naujojo Sodo G. 1',
        'Palangos oro uostas',
        'Kauno oro uostas',
        'Vilniaus oro uostas'
    ];

    $status = [
        'pending',
        'active',
        'complete'
    ];
    return [
        'date_from' => $faker->date(),
        'date_to' => $faker->date(),
        'place_from' =>  $place[rand(0, count($place) - 1)],
        'place_to' => $place[rand(0, count($place) - 1)],

        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone_nr' => $faker->phoneNumber,
        'address' => $faker->address,
        'city' => $faker->city,
        'country' => $faker->country,
        'birthday' => $faker->date(),
        'car_id' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(1, 200),
        'day_count' => $faker->numberBetween(1, 10),
        'status' => $faker->randomElement(['pending', 'active', 'completed'])
    ];
});
