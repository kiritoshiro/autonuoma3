<?php

use Faker\Generator as Faker;
use App\Car;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Car::class, function (Faker $faker) {

    $brands = [
        'Acura',
        'Audi',
        'Bentely',
        'BMW',
        'Buick',
        'Cadillac',
        'Chevrolet',
        'Chrystler',
        'Dodge',
        'Fiat',
        'Nissan',
        'Ford',
        'Honda',
        'Porsche',
        'Smart',
        'Opel'
    ];
    $models = [
        'Civic',
        'Accord',
        'Camry',
        'F-150',
        'Wrangler',
    ];
    $fuels = [
        'Benzinas',
        'Dyzelinas',
        'Dujos',
        'Elektra'
    ];
    $gearbox = [
        'Automatinė',
        'Rankinis',
    ];
    $color = [
        'Balta',
        'Juoda',
        'Raudona',
        'Geltona',
        'Žalia',
        'Mėlyna',
        'Pilka'
    ];

    return [
//        'user_id' => random_int(\DB::table('users')->min('id'), \DB::table('users')->max('id')),
        'brand' => $brands[rand(0, count($brands) - 1)],
        'day_price' => $faker->numberBetween(5, 100),
        'car_price' => $faker->numberBetween(300, 50000),
        'model' => $models[rand(0, count($models) - 1)],
        'fuels' => $fuels[rand(0, count($fuels) - 1)],
        'gearbox' => $gearbox[rand(0, count($gearbox) - 1)],

        'color' => $color[rand(0, count($color) - 1)],
        'years' => $faker->numberBetween(1980, 2017),
        'run' => $faker->numberBetween(0, 700000),
        'consumption' => $faker->numberBetween(1, 20),
        'description' => $faker->text(50),
        'img' => 'car.jpeg',
        'available' => $faker->boolean

    ];
});
