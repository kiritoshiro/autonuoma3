<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $users = factory(App\User::class, 10)->create();
        $admin[0] = ([
            'id' => 1,
            'name' => 'admin',
            'email' => 'admin@admin.admin',
            'password' => '$2y$10$3GSallkA/2JKlJCDkFN/Q.InfkWW/.6g8BETkQX7OLqR0n3JXRy92'
        ]);

        DB::table('users')->insert($admin);
    }
}
